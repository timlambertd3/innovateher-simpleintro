package co.uk.caci;

import co.uk.caci.models.Person;
import co.uk.caci.models.Office;

import java.util.Arrays;
import java.util.List;

/**
 * Tasks:
 *
 * 1. Add an age property to a person.
 * 2. Create another office.
 * 3. Add people to the other office.
 * 4. (Bonus Round) Sort the list of people in both of the offices using something like this:
 *          .sort((a, b) -> a.getFirstName().compareTo(b.getFirstName()))
 */
public class Main {

    /**
     * Main method, this is a special method in java that is the entry point for the program.
     * @param args The arguments passed into the program stored as an array of Strings.
     */
    public static void main(String[] args) {

        // Here we initialise a new Office object with an empty constructor (The constructor is the "new Office()"
        // section. Think of this as using the Office class as a template and now we're making an empty office.
        // this is then loaded into the variable "mediaCityOffice".
        Office mediaCityOffice = new Office();

        // Here we sett the address and office name of our "mediaCityOffice" variable.
        mediaCityOffice.setAddress("Floor 4, The Landing, Media City, Salford");
        mediaCityOffice.setOfficeName("Media City");

        // Here we're creating 2 Person objects with arguments this time, this means we populate all the values a
        // Person object contains.
        Person personA = new Person("Tim", "Lambert");
        Person personB = new Person("Lydia", "Kennedy");

        // Naive way of loading staff into the office staff list.
        mediaCityOffice.setStaffList(Arrays.asList(personA, personB));

        mediaCityOffice.getStaffList().sort((a, b) -> a.getFirstName().compareTo(b.getFirstName()));

        // Calls the "showIf(Office office)" method defined on line 74 with our office as the parameter.
        showIf(mediaCityOffice);

        // Prints the "mediaCityOffice" object out to console.
        System.out.println(mediaCityOffice);
    }

    /**
     * Example of 2 of the ways to do for loops in Java.
     * @param staffList List of staff at an office.
     */
    public static void showForLoop(List<Person> staffList) {

        // For loops loop through until a condition is met, so in this case our condition is that as long as i is
        // smaller than the size of the staff list, we continue looping. In the background this starts at 0, does the
        // thing between the {} and then adds 1 to i (i++ just means add 1 to it). We keep adding 1 and doing the thing
        // inside {} until we meet our condition.
        //
        // Think of this as keeping count of how many people try to get into music venue with a cap of 100 people, you'd
        // start at 0 and every time someone came in you'd check their name off the list and add 1 to your count, when
        // you get to 99 (0-99 is 100 people) you'd finish the loop.

        for (int i = 0; i < staffList.size(); i++) {
            System.out.println(staffList.get(i).getFirstName());
        }

        // This is known as the "enhanced for loop" and basically means you do something for each element in a
        // collection.
        //
        // Put simply for every person in the staff list, print their first name.

        for (Person person : staffList) {
            System.out.println(person.getFirstName());
        }
    }

    /**
     * Method to show off if statement, will display how many people are in the office.
     * @param office The office to display staff numbers in.
     */
    public static void showIf(Office office) {

        // If the thing in the brackets is a true expression then do the next thing, if not (else) then do the second
        // thing.

        if (office.getStaffList().isEmpty()) {
            System.out.println("No Staff in office");
        } else {
            System.out.println(String.format("There are %s staff in the office", office.getStaffList().size()));
        }
    }
}
