package co.uk.caci.models;

import java.util.List;

// This is a class, think of it like a template to create an object like a mould for a printing press, we can instantiate
// this to create 1 Office Object, there can be as many Office Objects as we have resources for.
public class Office {

    // These are the attributes that belong to the class, in this case a staff list, an address and an office name.
    private List<Person> staffList;
    private String address;
    private String officeName;

    /**
     * Full constructor.
     * @param staffList List of employees at the office.
     * @param address Address of the office.
     * @param officeName Name of the office.
     */
    public Office(List<Person> staffList, String address, String officeName) {
        this.staffList = staffList;
        this.address = address;
        this.officeName = officeName;
    }

    /**
     * No-Args constructor.
     */
    public Office() {}

    @Override
    public String toString() {
        return "Office{" +
                "staffList=" + staffList +
                ", address='" + address + '\'' +
                ", officeName='" + officeName + '\'' +
                '}';
    }

    public List<Person> getStaffList() {
        return staffList;
    }

    public void setStaffList(List<Person> staffList) {
        this.staffList = staffList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }
}
